module.exports = function (server, db) {
    server.post('/event', function (req, res, next) {
        let body = typeof req.body == "string" ? JSON.parse(req.body) : req.body;
        let collection = db.collection('event_signups');
        if (!body || !body.firstName || !body.lastName || !body.email ||
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(body.email) || !body.eventDate) {
            res.send(400);
            return next();
        }
        collection.insert(body, function (err, result) {
            if (err) {
                res.send(500, err);
                return next();
            }
            res.send(200);
            return next();
        })
    });
};