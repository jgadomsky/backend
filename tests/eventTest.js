const expect = require('chai').expect;
const restify = require('restify-clients');
const mongoClient = require('mongodb').MongoClient
var client, dbConn;

before(function (done) {
    process.env.NODE_ENV = 'test';
    var config = require('../config')();
    require('../index');
    client = restify.createJsonClient({
        url: `http://localhost:${config.server.port}`
    });
    mongoClient.connect(`mongodb://${config.db.ip}:${config.db.port}/${config.db.name}`, function (err, db) {
        dbConn = db
        done();
    });
});

describe('Event post', function () {
    it('correctly signs up for an event', function (done) {
        const postData = {
            firstName: 'test',
            lastName: 'test',
            email: 'test@test.com',
            eventDate: '2017-07-07'
        };
        client.post('/event', postData, function (err, req, res, obj) {
            expect(res.statusCode).to.be.equal(200);
            let collection = dbConn.collection('event_signups');
            collection.find({}, {
                '_id': 0
            }).toArray(function (err, items) {
                expect(err).to.be.equal(null);
                expect(items.length).to.be.equal(1);
                expect(items[0]).to.be.deep.equal(postData);
                done();
            });
        });
    });

    it('validate missing lastName', function (done) {
        client.post('/event', {
            firstName: 'test',
            email: 'test@test.com',
            eventDate: '2017-07-07'
        }, function (err, req, res, obj) {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
    });

    it('validate incorrect email', function (done) {
        client.post('/event', {
            firstName: 'test',
            lastName: 'test',
            email: 'testtest.com',
            eventDate: '2017-07-07'
        }, function (err, req, res, obj) {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
    });
});

after(function () {
    let collection = dbConn.collection('event_signups');
    collection.remove();
    dbConn.close();
});