const config = require('./config')();
const restify = require('restify');
const mongoClient = require('mongodb').MongoClient
const mongoUrl = `mongodb://${config.db.ip}:${config.db.port}/${config.db.name}`;
const eventRoutes = require('./routes/event');
const corsMiddleware = require('restify-cors-middleware');
const cors = corsMiddleware({
    origins: ['http://localhost:3000']
})
const httpServer = restify.createServer();

mongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
        console.error(err);
        return;
    }
    console.log(`Connected correctly to ${mongoUrl}`);

    httpServer.use(cors.actual);
    httpServer.use(restify.plugins.bodyParser());

    eventRoutes(httpServer, db);

    httpServer.listen(config.server.port, function () {
        console.log('Server running at %s', httpServer.url);
    });

    var gracefulShutdown = function () {
        console.log("Shutting down...");
        db.close();
        process.exit();
    }
    process.on('SIGTERM', gracefulShutdown);
    process.on('SIGINT', gracefulShutdown);
});