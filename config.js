module.exports = function () {
    switch (process.env.NODE_ENV) {
        case 'test':
            return {
                server: {
                    port: 8081
                },
                db: {
                    ip: 'localhost',
                    port: 27017,
                    name: 'test'
                }
            };
        default:
            return {
                server: {
                    port: 8080
                },
                db: {
                    ip: 'localhost',
                    port: 27017,
                    name: 'rekrutacja'
                }
            }
    }
};